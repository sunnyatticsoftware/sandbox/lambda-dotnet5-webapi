# lambda-dotnet5-webapi

## Pre-requirements
This assumes the SDK .NET 5.0 installed and Docker for Windows and GitBash or any Linux-like console.

## Steps to create project
Install dotnet templates for Amazon Lambda
```
dotnet new -i Amazon.Lambda.Templates
```

Create lambda project for .NET 5 as a containerized web api
```
dotnet new serverless.image.AspNetCoreWebAPI -n LambdaDotNet5
```

Compile
```
dotnet build -c Release
```

Publish compiled artifacts
```
dotnet publish -c Release
```

Build docker image (at the project level)
```
docker build -t lambda-dotnet:latest .
```

Run the container providing the entrypoint (if it's not included in the Dockerfile)
```
docker run -p 9000:8080 lambda-dotnet "LambdaDotNet5::LambdaDotNet5.LambdaEntryPoint::FunctionHandlerAsync"
```

In a different terminal, send an event to the lambda function
```
curl -vX POST http://localhost:9000/2015-03-31/functions/function/invocations -d @test_request.json --header "Content-Type: application/json"
```
being the test_request.json the following:
```
{
  "resource": "/{proxy+}",
  "path": "/api/values",
  "httpMethod": "GET",
  "headers": null,
  "queryStringParameters": null,
  "pathParameters": {
    "proxy": "api/values"
  },
  "stageVariables": null,
  "requestContext": {
    "accountId": "AAAAAAAAAAAA",
    "resourceId": "5agfss",
    "stage": "test-invoke-stage",
    "requestId": "test-invoke-request",
    "identity": {
      "cognitoIdentityPoolId": null,
      "accountId": "AAAAAAAAAAAA",
      "cognitoIdentityId": null,
      "caller": "BBBBBBBBBBBB",
      "apiKey": "test-invoke-api-key",
      "sourceIp": "test-invoke-source-ip",
      "cognitoAuthenticationType": null,
      "cognitoAuthenticationProvider": null,
      "userArn": "arn:aws:iam::AAAAAAAAAAAA:root",
      "userAgent": "Apache-HttpClient/4.5.x (Java/1.8.0_102)",
      "user": "AAAAAAAAAAAA"
    },
    "resourcePath": "/{proxy+}",
    "httpMethod": "GET",
    "apiId": "t2yh6sjnmk"
  },
  "body": null
}
```